package com.mdprod.misterauto.model.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;


public class TaskRQ {

    @Expose
    @SerializedName("userId")
    private int userId;

    public TaskRQ(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Map<String, String> getHash() {

        Gson gson = new Gson();

        String json = gson.toJson(this);

        Type type = new TypeToken<Map<String, String>>() {
        }.getType();

        Map<String, String> map = gson.fromJson(gson.toJson(this), type);

        return map;
    }
}
