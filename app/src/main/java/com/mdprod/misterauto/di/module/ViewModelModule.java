package com.mdprod.misterauto.di.module;

import com.mdprod.misterauto.di.ViewModelKey;
import com.mdprod.misterauto.ui.userTasks.UserTasksViewModel;
import com.mdprod.misterauto.ui.usersList.UsersListViewModel;
import com.mdprod.misterauto.util.ViewModelFactory;

import javax.inject.Singleton;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(UsersListViewModel.class)
    abstract ViewModel bindUsersListViewModel(UsersListViewModel usersListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(UserTasksViewModel.class)
    abstract ViewModel bindUserTasksViewModel(UserTasksViewModel userTasksViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
