package com.mdprod.misterauto.di.component;

import android.app.Application;

import com.mdprod.misterauto.App;
import com.mdprod.misterauto.di.module.ActivityBindingModule;
import com.mdprod.misterauto.di.module.AppModule;
import com.mdprod.misterauto.di.module.RetrofitModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, AppModule.class, RetrofitModule.class, ActivityBindingModule.class})
public interface ApplicationComponent extends AndroidInjector<App> {

    void inject(App application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }
}
