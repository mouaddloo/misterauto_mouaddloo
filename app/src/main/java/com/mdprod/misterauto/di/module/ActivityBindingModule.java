package com.mdprod.misterauto.di.module;

import com.mdprod.misterauto.ui.userTasks.UserTasksActivity;
import com.mdprod.misterauto.ui.usersList.UsersListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract UsersListActivity bindUsersListActivity();

    @ContributesAndroidInjector
    abstract UserTasksActivity bindUserTasksActivity();
}
