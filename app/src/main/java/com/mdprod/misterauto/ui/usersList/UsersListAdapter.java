package com.mdprod.misterauto.ui.usersList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mdprod.misterauto.R;
import com.mdprod.misterauto.databinding.ItemUserBinding;
import com.mdprod.misterauto.model.responses.UserRS;

import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.ViewHolder> {

    private static final String TAG = UsersListAdapter.class.getSimpleName();

    private Context context;
    private List<UserRS> list;
    private OnItemClickListener onItemClickListener;

    public UsersListAdapter(Context context, List<UserRS> list, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemUserBinding binding;

        public ViewHolder(ItemUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final UserRS model, final OnItemClickListener listener) {
            binding.setModel(model);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getLayoutPosition());
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ItemUserBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_user, parent, false);

        return new ViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserRS item = list.get(position);
        holder.bind(item, onItemClickListener);
    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }else{
            return 0;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
