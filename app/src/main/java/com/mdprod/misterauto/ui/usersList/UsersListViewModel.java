package com.mdprod.misterauto.ui.usersList;

import com.mdprod.misterauto.base.BaseViewModel;
import com.mdprod.misterauto.model.responses.UserRS;
import com.mdprod.misterauto.network.ApiRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UsersListViewModel extends BaseViewModel {

    private MutableLiveData<List<UserRS>> usersResponseLiveData = new MutableLiveData<>();

    @Inject
    public UsersListViewModel(ApiRepository repository) {
        super(repository);
    }

    public LiveData<List<UserRS>> getUsersList() {
        return usersResponseLiveData;
    }

    public void getUsers() {
        loading.setValue(true);
        compositeDisposable.add(repository.getUsersList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<List<UserRS>>() {
                    @Override
                    public void onSuccess(List<UserRS> value) {
                        loading.setValue(false);
                        usersResponseLiveData.setValue(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loading.setValue(false);
                        error.setValue(e.toString());
                    }
                }));
    }

}
