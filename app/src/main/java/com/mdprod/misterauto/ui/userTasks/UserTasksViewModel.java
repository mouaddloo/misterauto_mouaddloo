package com.mdprod.misterauto.ui.userTasks;

import com.mdprod.misterauto.base.BaseViewModel;
import com.mdprod.misterauto.model.requests.TaskRQ;
import com.mdprod.misterauto.model.responses.TaskRS;
import com.mdprod.misterauto.network.ApiRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UserTasksViewModel extends BaseViewModel {

    private MutableLiveData<List<TaskRS>> tasksResponseLiveData = new MutableLiveData<>();

    @Inject
    public UserTasksViewModel(ApiRepository repository) {
        super(repository);
    }
    public LiveData<List<TaskRS>> getUserTasks() {
        return tasksResponseLiveData;
    }

    public void getTasks(TaskRQ taskRQ) {
        loading.setValue(true);
        compositeDisposable.add(repository.getUserTasks(taskRQ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<List<TaskRS>>() {
                    @Override
                    public void onSuccess(List<TaskRS> value) {
                        loading.setValue(false);
                        tasksResponseLiveData.setValue(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loading.setValue(false);
                        error.setValue(e.toString());
                    }
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }
}
