package com.mdprod.misterauto.ui.userTasks;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mdprod.misterauto.R;
import com.mdprod.misterauto.databinding.ActivityUserTasksBinding;
import com.mdprod.misterauto.databinding.ActivityUsersListBinding;
import com.mdprod.misterauto.model.requests.TaskRQ;
import com.mdprod.misterauto.model.responses.TaskRS;
import com.mdprod.misterauto.model.responses.UserRS;
import com.mdprod.misterauto.ui.usersList.UsersListAdapter;
import com.mdprod.misterauto.ui.usersList.UsersListViewModel;
import com.mdprod.misterauto.util.Constants;
import com.mdprod.misterauto.util.ViewModelFactory;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.DaggerAppCompatActivity;

public class UserTasksActivity extends DaggerAppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;
    UserTasksViewModel userTasksViewModel;
    ActivityUserTasksBinding binding;
    List<TaskRS> taskRSList;
    int userId;
    TaskRQ taskRQ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_tasks);
        userTasksViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserTasksViewModel.class);
        binding.setViewmodel(userTasksViewModel);

        userId = getIntent().getIntExtra(Constants.USER_ID_TAG,0);
        taskRQ = new TaskRQ(userId);

        updateToolbar();
        ObserverViewModel();
        userTasksViewModel.getTasks(taskRQ);

    }

    public void updateToolbar(){
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getResources().getString(R.string.user_id)+userId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void ObserverViewModel() {
        userTasksViewModel.getLoading().observe(this, isLoading -> {
            if (isLoading) {
                binding.progress.setVisibility(View.VISIBLE);
            } else {
                binding.progress.setVisibility(View.GONE);
            }
        });

        userTasksViewModel.getUserTasks().observe(this, taskRs -> {
            taskRSList = taskRs;
            UserTasksAdapter adapter = new UserTasksAdapter(this, taskRs);
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            binding.recyclerView.setAdapter(adapter);
        });

        userTasksViewModel.getError().observe(this, error -> {
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        });
    }

}