package com.mdprod.misterauto.ui.userTasks;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mdprod.misterauto.R;
import com.mdprod.misterauto.databinding.ItemTaskBinding;
import com.mdprod.misterauto.databinding.ItemUserBinding;
import com.mdprod.misterauto.model.responses.TaskRS;
import com.mdprod.misterauto.model.responses.UserRS;

import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class UserTasksAdapter extends RecyclerView.Adapter<UserTasksAdapter.ViewHolder> {

    private static final String TAG = UserTasksAdapter.class.getSimpleName();

    private Context context;
    private List<TaskRS> list;

    public UserTasksAdapter(Context context, List<TaskRS> list) {
        this.context = context;
        this.list = list;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemTaskBinding binding;

        public ViewHolder(ItemTaskBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final TaskRS model) {
            binding.setModel(model);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ItemTaskBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_task, parent, false);

        return new ViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TaskRS item = list.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }else{
            return 0;
        }
    }
}
