package com.mdprod.misterauto.ui.usersList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.mdprod.misterauto.R;
import com.mdprod.misterauto.databinding.ActivityUsersListBinding;
import com.mdprod.misterauto.model.responses.UserRS;
import com.mdprod.misterauto.ui.userTasks.UserTasksActivity;
import com.mdprod.misterauto.util.Constants;
import com.mdprod.misterauto.util.ViewModelFactory;

import java.util.List;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.DaggerAppCompatActivity;

public class UsersListActivity extends DaggerAppCompatActivity implements UsersListAdapter.OnItemClickListener {

    @Inject
    ViewModelFactory viewModelFactory;
    UsersListViewModel usersListViewModel;
    ActivityUsersListBinding binding;
    List<UserRS> userRSList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_users_list);
        usersListViewModel = ViewModelProviders.of(this, viewModelFactory).get(UsersListViewModel.class);

        binding.setViewmodel(usersListViewModel);
        ObserverViewModel();
        usersListViewModel.getUsers();

    }

    private void ObserverViewModel() {
        usersListViewModel.getLoading().observe(this, isLoading -> {
            if (isLoading) {
                binding.progress.setVisibility(View.VISIBLE);
            } else {
                binding.progress.setVisibility(View.GONE);
            }
        });

        usersListViewModel.getUsersList().observe(this, userRS -> {
            userRSList = userRS;
            UsersListAdapter adapter = new UsersListAdapter(this, userRS, this);
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            binding.recyclerView.setAdapter(adapter);
        });

        usersListViewModel.getError().observe(this, error -> {
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        });
    }


    @Override
    public void onItemClick(int position) {
        if (userRSList != null){
            Intent intent = new Intent(getBaseContext(), UserTasksActivity.class);
            intent.putExtra(Constants.USER_ID_TAG, userRSList.get(position).getId());
            startActivity(intent);
        }
    }

}