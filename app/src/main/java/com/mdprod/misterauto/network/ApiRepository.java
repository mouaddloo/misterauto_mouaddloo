package com.mdprod.misterauto.network;

import com.mdprod.misterauto.model.requests.TaskRQ;
import com.mdprod.misterauto.model.responses.TaskRS;
import com.mdprod.misterauto.model.responses.UserRS;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.Single;

public class ApiRepository {

    ApiService apiService;

    @Inject
    public ApiRepository(ApiService apiService){
        this.apiService =  apiService;
    }

    public Single<List<UserRS>> getUsersList(){
        return apiService.getUsersList();
    }

    public Single<List<TaskRS>> getUserTasks(TaskRQ taskRQ){
        return apiService.getUserTasks(taskRQ.getHash());
    }


}
