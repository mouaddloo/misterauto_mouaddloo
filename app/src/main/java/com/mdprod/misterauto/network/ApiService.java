package com.mdprod.misterauto.network;

import com.mdprod.misterauto.model.responses.TaskRS;
import com.mdprod.misterauto.model.responses.UserRS;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiService {

    @GET("users")
    Single<List<UserRS>> getUsersList();

    @GET("todos")
    Single<List<TaskRS>> getUserTasks(@QueryMap Map<String, String> fields);
}
