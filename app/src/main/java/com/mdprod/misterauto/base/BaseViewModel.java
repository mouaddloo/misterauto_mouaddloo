package com.mdprod.misterauto.base;

import com.mdprod.misterauto.network.ApiRepository;


import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel extends ViewModel {

    public MutableLiveData<String> error = new MutableLiveData<>();
    public MutableLiveData<Boolean> loading = new MutableLiveData<>();
    public CompositeDisposable compositeDisposable;
    public ApiRepository repository;

    @Inject
    public BaseViewModel(ApiRepository repository) {
        this.repository = repository;
        this.compositeDisposable = new CompositeDisposable();
    }

    public LiveData<String> getError() {
        return error;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }

}
